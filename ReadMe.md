# JavaScript Docker Expres API
Simple project to illustrate Docker and Express.

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run API with Nodemon: `npm start`

# Endpoints
- GET /message
- GET /health
- GET /toggle

# Docker Worflow
- Use semantic versioning or simply 'latest'
- `docker build -t express-api:latest .`
- `docker tag express-api:latest mburolla/express-api:latest`
- `docker login`
- `docker push mburolla/express-api:latest`
- Pull and test using Docker Desktop
