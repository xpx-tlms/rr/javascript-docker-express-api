const cors = require('cors')
const express = require('express')

let healthStatus = true
const PORT = 5150
const app = express()
const corsOptions = { origin: ['http://localhost:3000'], optionsSuccessStatus: 200 }

// Middleware...
app.use(cors())
app.use(express.json())
app.use(express.urlencoded())

//
// GET /message
//

app.get('/message', cors(corsOptions), async (req, res) => { 
    // let id = req.params['id'];                 // Read params from URL.
    // let queryParam1 = req.query['personType']  // Read query params from URL.
    // let body = req.body;                       // Read request body.
    // res.send(<YOUR OBJECT HERE>);
    res.send({message: 'Hello World'});
});

//
// GET /message
//

app.get('/health', cors(corsOptions), async (req, res) => { 
    healthStatus ? res.send({message: 'OK'}) : res.send({message: 'DOWN'})
});

//
// GET /toggle
//

app.get('/toggle', cors(corsOptions), async (req, res) => { 
    healthStatus = !healthStatus;
    const hs = healthStatus ? 'HEALTHY' : 'UNHEALTHY';
    res.send({message : "Health status toggled to: " + hs});
});

app.listen(PORT, () => {
    console.log(`Express web API running on port: ${PORT}`);
})
